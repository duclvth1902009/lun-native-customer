import packageApi from '../../api/packageApi';
export const FETCH_PRODUCTS = 'FETCH_PRODUCTS';
export const PRODUCT_LOADING = 'PRODUCT_LOADING';
export const PRODUCT_FAILURE = 'PRODUCT_FAILURE';

export const fetchProducts = () => {
	return async (dispatch) => {
		dispatch({
			type: PRODUCT_LOADING,
		});
		// eslint-disable-next-line no-useless-catch
		try {
			const response = await packageApi.getAll(0);

			if (!response) {
				dispatch({
					type: PRODUCT_FAILURE,
				});
				throw new Error('Something went wrong!, can\'t get the products');
			}

			const resData = await response;
			dispatch({
				type: FETCH_PRODUCTS,
				products: resData,
			});
		} catch (err) {
			throw err;
		}
	};
};
