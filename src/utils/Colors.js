export default {
	primary: '#2da7bc',
	text: '#707070',
	grey: '#b5b5b5',
	blue: '#3c82fc',
	white: '#fff',
	light_grey: '#f8f8f8',
	dark: '#2a2c5c',
	bg: '#0f7e4a',
	yellow: '#fed922',
	light_bg: '#e0e0e2',
	light_green: '#f292a2', // Text color
	lighter_green: '#f292a2',
	leave_green: '#f292a2', // Category title
	purple: '#876aba',
	water: '#6f8bc8',
	green: '#1baa43',
	straw: '#e55d4b',
	red: '#e53b32',
	blue_light: '#1A91DA',
	blue_green: '#2CB9B0',
	black: '#000000',
};
