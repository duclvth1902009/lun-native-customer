export const categories = [
	{
		id : '60fac94087b5846339c5a45a',
		name : 'Tiệc Sinh Nhật',
		bg: require('../../../assets/Images/bg1.jpg'),
	},
	{
		id : '60fac94b87b5846339c5a45b',
		name : 'Tiệc Cưới',
		bg: require('../../../assets/Images/bg2.jpg'),
	}
];
// [ {
// 	id : '60fac94087b5846339c5a45a',
// 	name : 'Tiệc Sinh Nhật'
// }, {
// 	id : '60fac94b87b5846339c5a45b',
// 	name : 'Tiệc Cưới'
// }, {
// 	id : '60ffc19062ad032e66935c87',
// 	name : 'Tiệc Tân Gia'
// }, {
// 	id : '60ffc19662ad032e66935c88',
// 	name : 'Tiệc Bạn Bè'
// }, {
// 	id : '60ffc1a062ad032e66935c89',
// 	name : 'Tiệc Tròn Tuổi'
// }, {
// 	id : '60ffc1a862ad032e66935c8a',
// 	name : 'Tiệc Khai Trương'
// } ];