import React from 'react';
import moment from 'moment';
import 'moment/min/locales';
import WebView from 'react-native-webview';

moment.locale('vi');

export const WebViewOrder = () => {
	return <WebView
		source={{ uri: 'http://192.168.0.103:3000/tracking-orders-auth'}}
	/>;
};


export default WebViewOrder;
