export const comments = [
	{
		id: 1,
		username: 'Lionel Nguyen',
		content: 'Tuyệt!!!',
	},
	{
		id: 2,
		username: 'Kim Tinh',
		content:
      'Bữa tiệc tuyệt vời. Cảm ơn Shop nhiều',
	},
	{
		id: 3,
		username: 'Nam Thọ',
		content:
      'Năm sau sẽ lại nhờ cửa hàng plan cho birthday party của bé nhà mình',
	},
	{
		id: 4,
		username: 'Nguyên Hưng',
		content: 'Cho 5 sao, hàng đẹp xịn xò',
	},
	{
		id: 5,
		username: 'Hà Bò',
		content: 'Vừa đặt ở đây, các bạn nhân viên rất thân thiện, nhiệt tình support, bé nhà mình rât vui',
	},
	{
		id: 6,
		username: 'Khiêm',
		content: 'chất lượng đúng như quảng cảo',
	},
];
export default comments;
