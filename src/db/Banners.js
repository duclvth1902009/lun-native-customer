const banners = [
	{
		id: '9471645183',
		imageUrl: require('../assets/Images/banner1.jpg'),
	},
	{
		id: '5656210978',
		imageUrl: require('../assets/Images/banner6.jpg'),
	},
	{
		id: '044416421',
		imageUrl: require('../assets/Images/banner3.jpg'),
	}
];

export default banners;
