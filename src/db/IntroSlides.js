//slide
const slides = [
	{
		id: 1,
		lable: 'Lựa chọn gói dịch vụ',
		subtitle: 'Tìm gói dịch vụ yêu thích',
		des:
      'Việc lựa chọn gói dịch vụ yêu thích trở nên dễ dàng hơn bao giờ hết',
		imageUrl: require('../assets/Images/slide1.jpg'),
	},
	{
		id: 2,
		lable: 'Gói dịch vụ đa dạng và chất lượng',
		subtitle: 'Sản phẩm chất lượng',
		des:
      'Sự hài lòng về chất lượng gói dịch vụ của quý khách là sứ mệnh của chúng tôi',
		imageUrl: require('../assets/Images/slide2.jpg'),
	},
	{
		id: 3,
		lable: 'Mang hạnh phúc về',
		subtitle: 'Bạn còn chần chừ gì nữa?',
		des: 'Hãy tìm ngay cho một gói dịch vụ yêu thích tại lun decor ',
		imageUrl: require('../assets/Images/slide3.jpg'),
	},
];
export default slides;
