// api/axiosClient.js
import axios from 'axios';
import queryString from 'query-string';
import * as Config from '../utils/Config';
// Set up default config for http requests here

// Please have a look at here `https://github.com/axios/axios#request-config for the full list of configs

let axiosClient;
axiosClient = axios.create({
	// eslint-disable-next-line no-undef
	baseURL: Config.API_URL,
	headers: {
		'content-type': 'application/json',
		'apikey': Config.API_KEY
	},
	paramsSerializer: params => queryString.stringify(params),
});
axiosClient.interceptors.request.use(async (config) => {
	// Handle token here ...
	return config;
});
axiosClient.interceptors.response.use((response) => {
	if (response && response.data) {
		return response.data;
	}
	return response;
}, (error) => {
	// Handle errors
	throw error;
});
export default axiosClient;
