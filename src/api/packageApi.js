import axiosClient from './axiosClient';

const packageApi = {
	getAll: (atPageNum) => {
		const url = `/package?page=${atPageNum}&size=20&sort=id,ASC`;
		return axiosClient.get(url).then(res=>{
			return res;
		});
	},

	getById: (id) => {
		const url = `/package/${id}`;
		return axiosClient.get(url).then(res => {
			return res;
		});
	},

	getAllType: () => {
		const url = '/eventType';
		return axiosClient.get(url).then(res => {
			return res;
		});
	},
	getByType: (type) => {
		const url = '/package/eventType?eventType=' + type;
		return axiosClient.get(url).then(res => {
			console.log(res);
			return res;
		});
	}
};

export default packageApi;
