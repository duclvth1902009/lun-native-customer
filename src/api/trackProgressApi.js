import axiosClient from './axiosClient';

const tracksProgressApi = {
	getTracksByProjectId: (projectId, phoneNumber, OTPCode) => {
		const url = `/project/progress?id=${projectId}&phone=${phoneNumber}&otp=${OTPCode}`;
		return axiosClient.get(url);
	}
};

export default tracksProgressApi;
