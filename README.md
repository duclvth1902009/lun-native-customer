## Luna native App

## Get Started
 * Install app [expo](!https://play.google.com/store/apps/details?id=host.exp.exponent&hl=vi&gl=US) for android
 * Install app [expo](!https://apps.apple.com/us/app/expo-go/id982107779) for Ios

```bash
# install expo cli
npm install -g expo-cli
```
``` bash
# install dependencies
npm install
```
``` bash
# run project
npm start
```
``` bash
# run project on device
Scan QR code from expo app
```
``` bash
Change the API in config.js to the server that you're running. 
If you use my backend project the IP should be the IPv4 Address on your computer (cmd -> ipconfig)
```

## Features
- Authentication (Signup, Login, Reset Password).
- Login with Touch/Face ID.
- Real time update.
- User Profile (Upload, Edit Profile Picture, Address).
- Lottie Animation Icon (https://lottiefiles.com/)
- Header Animation.
- Add Items to Cart, to wishlist. 
- Place an Order.
- Payment Methods:  cash, credit card.
- Push notification to user whenever order status, user information change. 
- Send email for reseting password as well as update order information.
- Share Products to Social Media.
- Review, Comment, Rating Product (In Processing)

## Technical details
- React Native
- Expo managed workflows.
- UI framework: Reac Native Paper
- Intro slides: Animated, onScroll Event for animation.
- Header Animation: Animated, React Animatable.
- Form: Redux form validation.
- Icon: Lottie, React native vector icon.
- Payment: React native credit card, Stripe server for card validation checking.
- Loader: Skeleton loader, Linear gradient.
- Reducer: Redux. 
- Image Picker: Expo image picker.
- Deep Link: Expo Linking.
- Touch/Face ID: react native touch id, react native keychain, expo authentication

### Demo
<div style="display: flex; flex-wrap: wrap">
 <img src=""  width="250">
 <img src=""  width="250">
</div>
<div style="display: flex; flex-wrap: wrap">
  <img src="" width="250">
 <img src="" width="250">
</div>
<div style="display: flex">
 <img src="" width="250">
 <img src="" width="250">
</div>


### Fix lint config 
To fix the dependency tree, try following [these steps](!https://www.npmjs.com/package/eslint-config-react-app) in the exact order:

Delete package-lock.json (not package.json!) and/or yarn.lock in your project folder.
Delete node_modules in your project folder.
Remove "babel-eslint" from dependencies and/or devDependencies in the package.json file in your project folder.
Run npm install or yarn, depending on the package manager you use.



