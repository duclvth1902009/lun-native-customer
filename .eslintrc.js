module.exports = {
	env: {
		browser: true,
		es2021: true,
		es6: true,
		commonjs: true,
	},
	extends: ['eslint:recommended', 'plugin:react/recommended'],
	parser: 'babel-eslint',
	parserOptions: {
		'ecmaVersion': 2018,
		'sourceType': 'module',
		'ecmaFeatures': {
			'jsx': true
		}
	},
	plugins: ['react', 'import', 'react-hooks'],
	rules: {
		'react/jsx-uses-react': 1,
		'max-len': [1,1200,2,{'ignoreComments': true}],
		'no-console':[1],
		'no-unused-vars':[1],
		'react/jsx-uses-vars': [2],
		'no-loop-func':[1],
		'indent': [
			'error',
			'tab'
		],
		'linebreak-style': [
			'error',
			'unix'
		],
		'quotes': [
			'error',
			'single'
		],
		'semi': [
			'error',
			'always'
		],
    'no-mixed-spaces-and-tabs': 0,
    'react/prop-types': 0
	},
};
